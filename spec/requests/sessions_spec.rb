require 'rails_helper'

describe 'Sessions', type: :request do
  describe "POST /api/sessions" do
    let(:user) { create(:user) }

    it "creates valid token when credentials are correct and expires in 1 hour" do
      post '/api/sessions', params: { email: user.email, password: 'Password1!' }
      expect(response).to have_http_status(:ok)
      token = JSON.parse(response.body)['token']
      expect(token).to be_present

      get '/api/user', headers: { 'Authorization': "Bearer #{token}" }
      expect(response).to have_http_status(:ok)

      Timecop.travel(1.hour.from_now) do
        get '/api/user', headers: { 'Authorization': "Bearer #{token}" }
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end
end