require 'rails_helper'

describe 'Users', type: :request do
  describe "POST /api/user" do
    it "creates user" do
      email = 'fake@fake.com'
      post '/api/user', params: { user: { email: 'fake@fake.com', password: 'Password1!', password_confirmation: 'Password1!' } }
      expect(response).to have_http_status(:created)
      expect(User.find_by(email: email).present?).to be true
    end

    it "fails when creating a new user with an existing user's email" do
      user = create(:user)
      post '/api/user', params: { user: { email: user.email, password: 'Password1!', password_confirmation: 'Password1!' } }
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end

  describe "GET /api/user" do
    let(:user) { create(:user) }
    let(:game1) { create(:game) }
    let(:game2) { create(:game) }

    it "shows user stats when authenticated" do
      create_list(:game_event, 5, user: user, game: game1)
      create_list(:game_event, 2, user: user, game: game2)
      get '/api/user', headers: { 'Authorization': "Bearer #{user_login_token(user)}" }
      expect(response).to have_http_status(:ok)
      parsed_body = JSON.parse(response.body)['user']
      expect(parsed_body['id']).to eq(user.id)
      expect(parsed_body['email']).to eq(user.email)
      expect(parsed_body['stats']['total_games_played']).to eq(7)
    end

    it "does not show user stats when unauthenticated" do
      get '/api/user', headers: { 'Authorization': "Bearer somethingsomething" }
      expect(response).to have_http_status(:unauthorized)
    end
  end
end