require 'rails_helper'

describe 'GameEvents', type: :request do
  describe "POST /api/user/game_events" do
    let(:user) { create(:user) }
    let(:game) { create(:game) }
    let(:occurred_at) { '2024-06-25 22:40:35.088545000 UTC +00:00' }

    it "creates game events when params are valid" do
      post '/api/user/game_events', headers: { 'Authorization': "Bearer #{user_login_token(user)}" }, params: { game_event: { game_id: game.id, type: 'COMPLETED', occurred_at: occurred_at } }
      expect(response).to have_http_status(:created)
      game_event = user.reload.game_events.last
      expect(game_event.game_id).to eq(game.id)
      expect(game_event.occurred_at).to eq(occurred_at)
      expect(game_event.completed?).to be(true)
    end

    it "does not create game events when params are invalid" do
      post '/api/user/game_events', headers: { 'Authorization': "Bearer #{user_login_token(user)}" }, params: { game_event: { game_id: game.id, type: 'COMPLETED' } }
      expect(response).to have_http_status(:unprocessable_entity)
      expect(user.reload.game_events.size).to eq(0)
    end

    it "does not create an entity when user in unauthenticated" do
      post '/api/user/game_events', headers: { 'Authorization': "Bearer somethingsomething" }, params: { game_event: { game_id: game.id, type: 'COMPLETED', occurred_at: occurred_at } }
      expect(response).to have_http_status(:unauthorized)
      expect(user.reload.game_events.size).to eq(0)
    end
  end
end