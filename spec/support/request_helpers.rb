module RequestHelpers
  def user_login_token(user, expiration = nil)
    secret = Rails.application.credentials.jwt_secret_key!
    encoding = 'HS512'
    JWT.encode({ user_id: user.id, exp: expiration || 1.hour.from_now.to_i }, secret, encoding)
  end
end