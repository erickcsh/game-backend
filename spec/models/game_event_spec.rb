require 'rails_helper'

describe GameEvent, type: :model do
  it "is valid with valid attributes" do
    game = create(:game)
    user = create(:user)
    expect(GameEvent.new(game: game, user: user, occurred_at: "2024-06-25 22:40:35.088545000 UTC +00:00", type: 'COMPLETED')).to be_valid
  end

  it "is invalid without occurred)at" do
    game = create(:game)
    user = create(:user)
    expect(GameEvent.new(game: game, user: user, occurred_at: "", type: 'COMPLETED')).to be_invalid
  end

  it "is invalid with unexpected type" do
    expect { GameEvent.new(type: 'PROGRESS') }.to raise_error(ArgumentError)
  end
end