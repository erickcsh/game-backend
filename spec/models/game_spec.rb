require 'rails_helper'

describe Game, type: :model do
  it "is valid with valid attributes" do
    expect(Game.new(name: 'Game name')).to be_valid
  end

  it "is invalid without a name" do
    expect(Game.new).to be_invalid
  end

  it "is invalid when name already exists" do
    game = create(:game)
    expect(Game.new(name: game.name)).to be_invalid
  end
end