require 'rails_helper'

describe User, type: :model do
  EMAIL = 'fake@fake.com'
  STRONG_PASSWORD = 'Password1!'

  it "is valid with valid attributes" do
    expect(User.new(email: EMAIL, password: STRONG_PASSWORD, password_confirmation: STRONG_PASSWORD)).to be_valid
  end

  it "is invalid with a weak password" do
    expect(User.new(email: EMAIL, password: "password", password_confirmation: "password")).to be_invalid
  end

  it "is invalid when password confirmation does not match" do
    expect(User.new(email: EMAIL, password: STRONG_PASSWORD, password_confirmation: "Password!1")).to be_invalid
  end

  it "is invalid with erroneous email" do
    expect(User.new(email: "fake.com.", password: STRONG_PASSWORD, password_confirmation: STRONG_PASSWORD)).to be_invalid
  end
end