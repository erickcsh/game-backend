FactoryBot.define do
  factory :game_event do
    association :user
    association :game
    type { :completed }
    occurred_at { "2024-06-25 22:40:35.088545000 UTC +00:00" }
  end
end
