FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "test#{n}@fake.com" }
    password { 'Password1!' }
  end
end
