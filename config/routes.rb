Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  # Defines the root path route ("/")
  # root "posts#index"
  namespace :api do
    scope module: :v1 do
      resource :sessions, only: %i[create]

      get 'user', to: 'users#show'
      post 'user', to: 'users#create'
      post 'user/game_events', to: 'game_events#create'
    end
  end
end
