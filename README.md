# README

This app is a Coding Assessment.
This is a RESTful backend for an existing app that allows a user to sign up, sign in, track game completion events and consult their stats.

## System Requirements

* Ruby version: 3.3.1
* Postgresql version: 14.12
* Bundler: 2.5.0

## Set up the Project

### Run PostgreSQL

1. Validate Postgresql is running (in macOS you can use hombrew: `brew services start postgresql@14`)

### Install bundler and gems

1. Install bundler: `gem install bundler`
1. Run `bundle install`

### Set up the database

1. Run `bundle exec rails db:create`
1. Run `bundle exec rails db:migrate`
1. Run `bundle exec rails db:seed`

### Create JWT secret key

1. Generate a secret key, one way to generate it if you have node installed is by running `node -e "console.log(require('crypto').randomBytes(32).toString('hex'))"`
1. Add the value from step 1 using the key `jwt_secret_key` to the local credentials by running `bundle exec rails credentials:edit`

### Run the local server

1. Run the project by executing `bundle exec rails s`

## Example requests

### Sign Up a User

Replace <email> and <password> accordingly.

```
curl --location --request POST 'localhost:3000/api/user' \
--header 'Content-Type: application/json' \
--data-raw '{
    "user": {
        "email": "<email>",
        "password": "<pasword>",
        "password_confirmation": "<password>"
    }
}'
```

### Sign In a User

Replace <email> and <password> accordingly.

```
curl --location --request POST 'localhost:3000/api/sessions' \
--header 'Content-Type: application/json' \
--data-raw '{
    "email": "<email>",
    "password": "<password>"
}'
```

### Report Game Completion Event

The token for the header is retrieved from the 'Sign In a User' response.
Replace game_id and occurred_at accordingly.

```
curl --location --request POST 'localhost:3000/api/user/game_events' \
--header 'Authorization: Bearer <token here>' \
--header 'Content-Type: application/json' \
--data-raw '{
    "game_event": {
        "type": "COMPLETED",
        "game_id": 2,
        "occurred_at": "2024-06-25 22:40:35.088545000 UTC +00:00"
    }
}'
```

### Retrieve User Details and Stats

The token for the header is retrieved from the 'Sign In a User' response.

```
curl --location --request GET 'localhost:3000/api/user' \
--header 'Authorization: Bearer <token here>'
```

## Running tests

To run all tests run `rspec`

To run a single test file run `rspec /spec/requests/sessions_spec.rb`