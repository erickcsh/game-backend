class UserSerializer
  include JSONAPI::Serializer
  attributes :id, :email, :stats
end
