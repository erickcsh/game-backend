class User < ApplicationRecord
  # TBD: Password requirements
  # taken from https://stackoverflow.com/questions/5123972/ruby-on-rails-password-validation
  PASSWORD_FORMAT = /\A
    (?=.{8,})          # Must contain 8 or more characters
    (?=.*\d)           # Must contain a digit
    (?=.*[a-z])        # Must contain a lower case character
    (?=.*[A-Z])        # Must contain an upper case character
    (?=.*[[:^alnum:]]) # Must contain a symbol
  /x

  has_secure_password
  validates :email, uniqueness: true, email: true, presence: true
  validates :password, presence: true, format: { with: PASSWORD_FORMAT }, on: :create

  has_many :game_events, dependent: :destroy

  # TBD: Sticking this here to easily complete the interview assessment close enough to the proposed 2 hour timebox
  # However, this can become problematic especially since the client calls often the User#show endpoint to retrieve
  # these details.

  # These details won't change until a new game is completed, instead of reading this from the events table, which
  # will grow exponentially as users play multiple games this can be refactored to use:
  # - a cache store like Redis to store the stats for a user for a period of timebox
  # - a separate table to store the stats for the user condensed
  # - a database view or function to calculate the stats
  def stats
    total_games_played = game_events.completed.count
    { total_games_played: total_games_played }
  end
end
