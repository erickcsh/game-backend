class GameEvent < ApplicationRecord
  self.inheritance_column = 'sti_type'

  validates :occurred_at, presence: true
  validates :type, presence: true

  belongs_to :game
  belongs_to :user

  EVENT_TYPES = {
    completed: "COMPLETED",
  }

  enum type: EVENT_TYPES
end
