class Game < ApplicationRecord
  validates :name, presence: true, uniqueness: true

  has_many :game_events, dependent: :destroy
end
