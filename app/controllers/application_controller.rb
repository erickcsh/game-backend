class ApplicationController < ActionController::API
  # rescue_from StandardError, with: :error_occurred
  rescue_from ::ActiveRecord::RecordNotFound, with: :record_not_found

  before_action :authenticate_user!

  protected

  def encode_token(payload)
    # TBD: expire it in 1 hour for interview purposes
    payload[:exp] = 1.hour.from_now.to_i
    JWT.encode(payload, Rails.application.credentials.jwt_secret_key!, 'HS512')
  end

  def decoded_token
    header = request.headers['Authorization']
    if header
      token = header.split(" ")[1]
      begin
        decoded_token = JWT.decode(token, Rails.application.credentials.jwt_secret_key!, true, algorithm: 'HS512')
        decoded_token[0]['exp'] > Time.now.to_i ? decoded_token : nil
      rescue JWT::DecodeError
        nil
      end
    end
  end

  def current_user
    return @user if @user.present?

    token = decoded_token
    return nil if token.nil?

    user_id = token[0]['user_id']
    @user = User.find_by(id: user_id)
  end

  def authenticate_user!
    if current_user.nil?
      render status: :unauthorized
    end
  end

  def record_not_found(exception)
    render status: 404 and return
  end

  def error_occurred(exception)
    render status: 500 and return
  end
end
