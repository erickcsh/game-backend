module Api
  module V1
    class SessionsController < V1::ApiController
      skip_before_action :authenticate_user!, only: :create

      def create
        @user = User.find_by(email: params[:email])

        if @user.present? && @user.authenticate(params[:password])
          @token = encode_token(user_id: @user.id)
          render json: {
              token: @token # TBD: Is token the right key for the client?
          }, status: :ok
        else
          render json: { message: 'Invalid credentials' }, status: :unauthorized
        end
      end
    end
  end
end
