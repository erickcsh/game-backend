module Api
  module V1
    class UsersController < V1::ApiController
      skip_before_action :authenticate_user!, only: :create

      # To validate during the interview: { user: { email:, :password, :password_confirmation } is the expected body?
      def create
        user_params = params.require(:user).permit(:email, :password, :password_confirmation)

        @user = User.new(user_params)
        if @user.save
          render status: :created # TBD: What's the expected body response, if any
        else
          render json: { errors: @user.errors.full_messages }, status: :unprocessable_entity
        end
      end

      def show
        serialized_data = UserSerializer.new(@user).serializable_hash
        # Forcing this structure instead of usig the serializer since the client already requests data in this format
        render json: { user: serialized_data[:data][:attributes] }
      end
    end
  end
end
