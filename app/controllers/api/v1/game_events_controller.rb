module Api
  module V1
    class GameEventsController < V1::ApiController
      def create
        # TBD: What is the expected format for occurred at, right now the assumption is to have it
        # in the format of "2024 22:40:35.088545000 UTC +00:00"
        event_params = params.require(:game_event).permit(:type, :occurred_at, :game_id)
        # TBD: What if the same client tries to create the same event twice?
        @game_event = GameEvent.new(**event_params, user: current_user)
        if @game_event.save
          # TBD: Is it worth to perform an async update to the user stats here?
          render status: :created # TBD: What's the expected body response
        else
          render json: { errors: @game_event.errors.full_messages }, status: :unprocessable_entity
        end
      end
    end
  end
end
